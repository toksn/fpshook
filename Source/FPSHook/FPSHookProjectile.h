// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Actor.h"
#include "FPSHookProjectile.generated.h"

UCLASS(config=Game)
class AFPSHookProjectile : public AActor
{
	GENERATED_BODY()

public:
	enum HOOK_STATE{
		NONE,
		FLYING,
		HOOKED,
		WINCH_IN,
		WINCH_OUT
	} mCurrentState;


	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
	class USphereComponent* CollisionComp;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UProjectileMovementComponent* ProjectileMovement;

	// Called when the game starts or when spawned
	//virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	void RecalcLine();

	void TryUnfoldLine();

	void foldLineAtHit(FHitResult hit, int32 indexToInsert);
	void foldLineAtMultipleHits(FHitResult hit1, FHitResult hit2, int32 indexToInsert, FCollisionQueryParams traceParams);
	bool extendPointsUntilSight(FVector &pt1, FVector &pt2, FVector dir1, FVector dir2, FCollisionQueryParams traceParams, int32 maxIterations = 100);
	//void moveHitPointsForValidTrace(FHitResult hit1, FHitResult hit2, int32 indexToInsert, FCollisionQueryParams traceParams);

public:
	AFPSHookProjectile();

	void DrawHookLine();
	void ApplyPhysics(float DeltaSeconds);

	/** called when projectile hits something */
	UFUNCTION()
	void OnHit(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	void SetHookState(HOOK_STATE state);
	HOOK_STATE GetHookState();

	void SetOwner(AActor * OwningActor);

	/** Returns CollisionComp subobject **/
	FORCEINLINE class USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

	TArray<FVector> mHookLinePoints;
	float mCurrentMaxLength;
	AActor* mHitActor;
	AActor* mOwningActor;

	float mWinchInSpeed;
	float mTimeWinch;

	FColor mHookLineColor;
};

