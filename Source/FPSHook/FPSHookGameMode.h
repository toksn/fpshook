// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "FPSHookGameMode.generated.h"

UCLASS(minimalapi)
class AFPSHookGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AFPSHookGameMode();
};



