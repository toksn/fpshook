// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "FPSHook.h"
#include "FPSHookCharacter.h"
#include "FPSHookProjectile.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "FPSHookMovementComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AFPSHookCharacter
AFPSHookCharacter::AFPSHookCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UFPSHookMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	//MeshComp = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("Collectable"));
	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->AttachParent = GetCapsuleComponent();
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->AttachParent = FirstPersonCameraComponent;
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	//FP_Gun->AttachTo(Mesh1P, TEXT("GripPoint"), EAttachLocation::SnapToTargetIncludingScale, true);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->AttachTo(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 30.0f, 10.0f);

	mHook = NULL;
	mbUseInstantHitScan = true;

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P are set in the
	// derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	//mHookControlMode = HOOK_CONTROL_MODE::ADVANCED;
	mHookControlMode = HOOK_CONTROL_MODE::SIMPLE;
}


void AFPSHookCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	FP_Gun->AttachTo(Mesh1P, TEXT("GripPoint"), EAttachLocation::SnapToTargetIncludingScale, true); //Attach gun mesh component to Skeleton, doing it here because the skelton is not yet created in the constructor
}

//////////////////////////////////////////////////////////////////////////
// Input

void AFPSHookCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	// set up gameplay key bindings
	check(InputComponent);

	InputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AFPSHookCharacter::TouchStarted);
	if (EnableTouchscreenMovement(InputComponent) == false)
	{
		InputComponent->BindAction("Fire", IE_Pressed, this, &AFPSHookCharacter::OnFire);
		InputComponent->BindAction("Fire", IE_Released, this, &AFPSHookCharacter::OnReleaseFire);
		InputComponent->BindAction("AltFire", IE_Pressed, this, &AFPSHookCharacter::OnAltFire);
		InputComponent->BindAction("AltFire", IE_Released, this, &AFPSHookCharacter::OnReleaseAltFire);
		InputComponent->BindAction("ReleaseHook", IE_Pressed, this, &AFPSHookCharacter::StopHook);

		if (mHookControlMode == HOOK_CONTROL_MODE::ADVANCED)
		{
			InputComponent->BindAction("WinchIn", IE_Pressed, this, &AFPSHookCharacter::OnWinchIn);
			InputComponent->BindAction("WinchIn", IE_Released, this, &AFPSHookCharacter::OnReleaseWinchIn);

			InputComponent->BindAction("WinchOut", IE_Pressed, this, &AFPSHookCharacter::OnWinchOut);
			InputComponent->BindAction("WinchOut", IE_Released, this, &AFPSHookCharacter::OnReleaseWinchOut);	
		}
	}

	InputComponent->BindAxis("MoveForward", this, &AFPSHookCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AFPSHookCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &AFPSHookCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &AFPSHookCharacter::LookUpAtRate);
}

void AFPSHookCharacter::OnAltFire()
{
	if (mHookControlMode == HOOK_CONTROL_MODE::SIMPLE)
	{
		OnReleaseWinchIn();
		// start hooking
		StartHook();
	}
	else if (mHookControlMode == HOOK_CONTROL_MODE::ADVANCED)
	{
		// stop hooking
		StopHook();
	}
}

void AFPSHookCharacter::OnReleaseAltFire()
{	
	if (mHookControlMode == HOOK_CONTROL_MODE::SIMPLE)
	{
		// when not winching in --> stop hook (otherwise we prolly hold primary fire atm)
		if (mHook != NULL && mHook->GetHookState() != AFPSHookProjectile::HOOK_STATE::WINCH_IN)
			StopHook();
	}
	else
		StopHook();
}

void AFPSHookCharacter::OnFire()
{
	// hook_control_mode - advanced
		// fire -> spawn hook -> winch in
		// altfire -> despawn hook
		// winchOutKey -> winchOut
		// winchInKey -> winchIn
	// hook_control_mode - simple
		// fire -> spawn hook -> winch in -> despawn on release
		// altfire -> spawn hook -> hooked, no winching -> despawn on release
		// winchOutKey -> nothing
		// winchInKey -> nothing

	// set winch in hook state
	OnWinchIn();

	// start hooking
	StartHook();
}

void AFPSHookCharacter::OnReleaseFire()
{
	if (mHookControlMode == HOOK_CONTROL_MODE::SIMPLE)
	{
		// when not hooked state --> stop hook (otherwise we prolly hold secondary fire atm)
		if (mHook != NULL && mHook->GetHookState() != AFPSHookProjectile::HOOK_STATE::HOOKED)
			StopHook();
	}
	// unset winch in hook state
	OnReleaseWinchIn();
}

void AFPSHookCharacter::StartHook()
{
	// no hook currently fired/hooked -->
	// try and fire a projectile
	if (mHook == NULL)
	{
		if (ProjectileClass != NULL)
		{
			FRotator SpawnRotation = GetControlRotation();
			FVector SpawnLocation = FP_MuzzleLocation->GetComponentLocation();
			UWorld* const World = GetWorld();
			if (World != NULL)
			{
				if (mbUseInstantHitScan)
				{
					// raytrace to start at camera and go in camera direction instead of controlrotation/muzzleLocation
					// because when using raytracing the difference from the crosshair is very noticable
					SpawnLocation = FirstPersonCameraComponent->GetComponentLocation();
					SpawnRotation = FirstPersonCameraComponent->GetComponentRotation();

					// raytrace 3000.0 units
					float raytraceLength = 3000.0f;
					FHitResult pointForHook;

					FCollisionQueryParams trace(FName(TEXT("ComponentIsVisibleFrom")), false, this); // ignore this actor for trace collision
					if (World->LineTraceSingleByChannel(pointForHook, SpawnLocation, SpawnLocation + (SpawnRotation.Vector() * raytraceLength), ECC_GameTraceChannel1, trace))
					{
						// trace did hit something!
						// spawn the hook projectile at found hit location
						mHook = World->SpawnActor<AFPSHookProjectile>(ProjectileClass, pointForHook.Location, SpawnRotation);
						mHook->SetOwner(this);

						// imitate onHit method for hook
						mHook->OnHit(pointForHook.GetActor(), pointForHook.GetComponent(), pointForHook.Normal, pointForHook);
					}
					// UNDO: no need to spawn a projectile when not hit anything
					// 
					//// trace did not hit anything with the linetrace 
					//else
					//{
					//	// put the projectile at the end of the location
					//	SpawnLocation += (SpawnRotation.Vector() * raytraceLength);
					//	mHook = World->SpawnActor<AFPSHookProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
					//	mHook->SetOwner(this);
					//
					//	// set projectile speed to 0 / inactive
					//	mHook->GetProjectileMovement()->InitialSpeed = 0.0f;
					//	mHook->GetProjectileMovement()->SetActive(false);
					//}
				}
				else
				{
					// spawn the hook projectile
					mHook = World->SpawnActor<AFPSHookProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
					mHook->SetOwner(this);
				}
			}
		}
	}

}

void AFPSHookCharacter::OnWinchIn()
{
	mbWinchingIn = true;
	// hook already in use - winching in
	if (mHook)
	{
		mHook->SetHookState(AFPSHookProjectile::HOOK_STATE::WINCH_IN);
	}
}
void AFPSHookCharacter::OnReleaseWinchIn()
{
	mbWinchingIn = false;
	if (mHook != NULL)
	{
		mHook->SetHookState(AFPSHookProjectile::HOOK_STATE::HOOKED);
	}
}

void AFPSHookCharacter::OnWinchOut()
{
	mbWinchingOut = true;
	// hook already in use - winching out
	if(mHook)
	{
		mHook->SetHookState(AFPSHookProjectile::HOOK_STATE::WINCH_OUT);
	}
}
void AFPSHookCharacter::OnReleaseWinchOut()
{
	mbWinchingOut = false;
	if (mHook != NULL)
	{
		mHook->SetHookState(AFPSHookProjectile::HOOK_STATE::HOOKED);
	}
}

void AFPSHookCharacter::StopHook()
{
	// delete current hook?!
	if (mHook)
	{
		mHook->Destroy();
		mHook = NULL;
	}
}

void AFPSHookCharacter::OnHookHit(AFPSHookProjectile * hook)
{
	if(mbWinchingIn)
		hook->SetHookState(AFPSHookProjectile::HOOK_STATE::WINCH_IN);
	else if(mbWinchingOut)
		hook->SetHookState(AFPSHookProjectile::HOOK_STATE::WINCH_OUT);
	else
		hook->SetHookState(AFPSHookProjectile::HOOK_STATE::HOOKED);
}

void AFPSHookCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AFPSHookCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = false;
}

void AFPSHookCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
	{
		if (TouchItem.bIsPressed)
		{
			if (GetWorld() != nullptr)
			{
				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
				if (ViewportClient != nullptr)
				{
					FVector MoveDelta = Location - TouchItem.Location;
					FVector2D ScreenSize;
					ViewportClient->GetViewportSize(ScreenSize);
					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
					{
						TouchItem.bMoved = true;
						float Value = ScaledDelta.X * BaseTurnRate;
						AddControllerYawInput(Value);
					}
					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
					{
						TouchItem.bMoved = true;
						float Value = ScaledDelta.Y * BaseTurnRate;
						AddControllerPitchInput(Value);
					}
					TouchItem.Location = Location;
				}
				TouchItem.Location = Location;
			}
		}
	}
}

void AFPSHookCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AFPSHookCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AFPSHookCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AFPSHookCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool AFPSHookCharacter::EnableTouchscreenMovement(class UInputComponent* InputComponent)
{
	bool bResult = false;
	if (FPlatformMisc::GetUseVirtualJoysticks() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		bResult = true;
		InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AFPSHookCharacter::BeginTouch);
		InputComponent->BindTouch(EInputEvent::IE_Released, this, &AFPSHookCharacter::EndTouch);
		InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AFPSHookCharacter::TouchUpdate);
	}
	return bResult;
}
