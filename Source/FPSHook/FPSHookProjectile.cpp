// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "FPSHook.h"
#include "FPSHookProjectile.h"
#include "FPSHookCharacter.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "DrawDebugHelpers.h"


AFPSHookProjectile::AFPSHookProjectile()
{
	//SetActorTickEnabled(true);
	PrimaryActorTick.bCanEverTick = true;

	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &AFPSHookProjectile::OnHit);		// set up a notification for when this component hits something blocking

																					// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	//InitialLifeSpan = 3.0f;


	///////////************* MINE *******************//////////////
	mHitActor = NULL;
	mOwningActor = NULL;
	mCurrentState = HOOK_STATE::FLYING;

	// cfg
	mWinchInSpeed = 600.0f;
	mTimeWinch = 0.0f;
}

void AFPSHookProjectile::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// may be too early?
	//if (!mOwningActor)
	//	Destroy();

	mHookLineColor = FColor::Red;

	bool bDrawLines = false;
	bool bRecalcLine = false;
	bool bApplyPhysics = false;
	if (mCurrentState == HOOK_STATE::HOOKED || mCurrentState == HOOK_STATE::WINCH_IN || mCurrentState == HOOK_STATE::WINCH_OUT)
	{
		bDrawLines = true;
		bRecalcLine = true;
		bApplyPhysics = true;
	}
	else if (mCurrentState == HOOK_STATE::FLYING)
	{
		bDrawLines = true;
		bRecalcLine = true;
		mHookLineColor = FColor::Green;
	}

	if (bRecalcLine)
	{
		// recalc line - fold & unfold
		RecalcLine();
	}
	if (bApplyPhysics)
	{
		ApplyPhysics(DeltaSeconds);
	}

	if (bDrawLines)
	{
		DrawHookLine();
	}
}

// recalc line - trace from one point to another and check for hit objects to fold around them
void AFPSHookProjectile::RecalcLine()
{
	if (!mOwningActor)
		return;

	TryUnfoldLine();

	UWorld* const World = GetWorld();
	if (World != NULL)
	{
		FVector curPoint = mOwningActor->GetActorLocation();
		FVector nextPoint;

		// trace each part of the line and create another hooklinePoint for each hit
		FCollisionQueryParams trace(FName(TEXT("ComponentIsVisibleFrom")));
		trace.AddIgnoredActor(this); // ignore this actor for trace collision
		trace.AddIgnoredActor(mOwningActor); // ignore this owner for trace collision
		FHitResult collision1, collision2;

		for (int i = 0; i < mHookLinePoints.Num(); i++)
		{
			nextPoint = mHookLinePoints[i];
			// trace current line segment p1 - p2 
			if (World->LineTraceSingleByChannel(collision1, curPoint, nextPoint, ECC_GameTraceChannel1, trace))
			{
				// trace current line segment p2 - p1
				if (World->LineTraceSingleByChannel(collision2, nextPoint, curPoint, ECC_GameTraceChannel1, trace))
				{
					// both impact points are REALLY close to each other - they are probably the same
					if((collision1.ImpactPoint - collision2.ImpactPoint).Size() < 0.01)
						foldLineAtHit(collision1, i);
					else
						// fold line over multiple hits
						foldLineAtMultipleHits(collision1, collision2, i, trace);
				}
				
			}
				

			// move on to the next point
			curPoint = mHookLinePoints[i];
		}
		if (mCurrentState == HOOK_STATE::FLYING)
		{
			// do another last trace from current point to current projectile location
			if (World->LineTraceSingleByChannel(collision1, curPoint, this->GetActorLocation(), ECC_GameTraceChannel1, trace))
				foldLineAtHit(collision1, -1);
		}
	}
}

// try to unfold line segments
void AFPSHookProjectile::TryUnfoldLine()
{
	bool bAdditionalChecks = true;
	// not enough points to unfold anything
	if (!mOwningActor || mHookLinePoints.Num() < 2)
		return;

	UWorld* const World = GetWorld();
	if (World != NULL)
	{
		FVector curPoint = mOwningActor->GetActorLocation();

		
		FCollisionQueryParams trace(FName(TEXT("ComponentIsVisibleFrom")));
		trace.AddIgnoredActor(this); // ignore this actor for trace collision
		trace.AddIgnoredActor(mOwningActor); // ignore this owner for trace collision
		FHitResult collision;
		bool bCleanUnfold = false;

		// trace each two segments (p1-p2-p3) of the hookline skipping p2
		for (int i = 1; i < mHookLinePoints.Num(); i++)
		{
			bCleanUnfold = false;
			// trace p1 -> p3
			if (!World->LineTraceSingleByChannel(collision, curPoint, mHookLinePoints[i], ECC_GameTraceChannel1, trace))
			{
				// trace p1 -> p3 did NOT hit anything
				bCleanUnfold = true;
				
				if (bAdditionalChecks)
				{
					// additional check for hits near the corner of p1-p2-p3
					FVector dirP1P2 = mHookLinePoints[i - 1] - curPoint;
					FVector dirP2P3 = mHookLinePoints[i] - mHookLinePoints[i - 1];
					float fLineLengthP1P2 = dirP1P2.Size();
					float fLineLengthP2P3 = dirP2P3.Size();
					dirP1P2.Normalize();
					dirP2P3.Normalize();
					float fDistToCheck = 5.0f;
					
					if (fLineLengthP1P2 > fDistToCheck && fLineLengthP2P3 > fDistToCheck)
					{
						FVector p1p2 = mHookLinePoints[i - 1] - fDistToCheck * dirP1P2;
						FVector p2p3 = mHookLinePoints[i - 1] + fDistToCheck * dirP2P3;


						if (World->LineTraceSingleByChannel(collision, p1p2, p2p3, ECC_GameTraceChannel1, trace))
							bCleanUnfold = false;
						//else
						//	// debug draw line that went through without any obstacles
						//	DrawDebugLine(GetWorld(), p1p2, p2p3, FColor::Orange, false, 30.0f, 0, 1.0f);
					}
					// try 5 to 95% of the length 
					if (bCleanUnfold && fLineLengthP1P2 > 0.0f && fLineLengthP2P3 > 0.0f)
					{
						// 30 % steps
						float fPercent = 0.05;
						while (bCleanUnfold && fPercent < 1.0f)
						{
							FVector p1p2 = curPoint + fPercent * fLineLengthP1P2 * dirP1P2;
							FVector p2p3 = mHookLinePoints[i - 1] + fPercent * fLineLengthP2P3 * dirP2P3;

							if (World->LineTraceSingleByChannel(collision, p1p2, p2p3, ECC_GameTraceChannel1, trace))
								bCleanUnfold = false;

							fPercent += 0.3;
						}
					}
				}
				
				// when the trace from p1 to p3 did NOT hit anything -> remove p2
				if( bCleanUnfold )
					mHookLinePoints.RemoveAt(i - 1);
			}
			else
			{
				// todo: last line check may hit at the very end --> try to filter hits that are really close to end/start 
				//if(collision.bStartPenetrating)
			}

			// move on to the next point
			curPoint = mHookLinePoints[i-1];
		}
	}
}

void AFPSHookProjectile::foldLineAtHit(FHitResult hit, int32 indexToInsert)
{
	// only fold when the hit location is at least ~10units away from the start and endpoint
	if (hit.Distance < 10 || (hit.TraceEnd - hit.ImpactPoint).Size() < 10)
		return;

	// hit anything on this part of the line --> fold at the hit
	FVector newHookLinePoint = hit.ImpactPoint + 5 * hit.ImpactNormal;

	// debug draw line from location to actual new hooklinePoint
	DrawDebugLine(GetWorld(), hit.ImpactPoint, newHookLinePoint, FColor::Cyan, false, 30.0f, 0, 3.0f);
	
	// put the new point into the hookline array
	mHookLinePoints.Insert(newHookLinePoint, indexToInsert);
}

void AFPSHookProjectile::foldLineAtMultipleHits(FHitResult hit1, FHitResult hit2, int32 indexToInsert, FCollisionQueryParams traceParams)
{
	// situation:
	// traceStart(hit1)  ------>  hit1.impactPoint --- (may interect something) --> hit2.impactpoint --> traceEnd(hit1)
	//
	// in here we try to find new points that have nothing between them
	// we move them in impactNormal direction until they see eachother
	// another optimization is to try the cut the resulting line in half 
	// but make sure (original hit1.impactPoint) -> newHalfLinePoint -> (original hit2.impactPoint) dont intersect anything
	// otherwise just use the moved-out impactpoints that are able to see each other
	bool bTryToOptimizeResult = true;
	FColor colCollisionCorrection1 = FColor::Cyan;
	FColor colCollisionCorrection2 = FColor::Blue;
	FHitResult innerCollsion;

	if (hit1.Distance == 0.0f || hit2.Distance == 0.0f)
		return;
	//else if (hit1.Distance < 10)
	//	return foldLineAtHit(hit1, indexToInsert);
	//else if (hit2.Distance < 10)
	//	return foldLineAtHit(hit2, indexToInsert);

	UWorld* const World = GetWorld();
	if (World != NULL)
	{
		FVector dir1 = hit1.ImpactPoint - hit1.TraceStart;
		dir1.Normalize();
		dir1 += hit1.ImpactNormal;
		dir1.Normalize();

		FVector dir2 = hit2.ImpactPoint - hit2.TraceStart;
		dir2.Normalize();
		dir2 += hit2.ImpactNormal;
		dir2.Normalize();

		
		FVector movedImpactPoint1 = hit1.ImpactPoint;
		FVector movedImpactPoint2 = hit2.ImpactPoint;
				
		// try to extend the impact points in the calculated direction until they can trace eachother, max 100 iterations
		if (!extendPointsUntilSight(movedImpactPoint1, movedImpactPoint2, dir1, dir2, traceParams, 100) )
		{
			//only try it once
			//return;

			// there may be special cases where dir1 == dir2 (or really really close), still need endless loop protection in this case too.
			// 2 more attempts should be more than enough, otherwise its prolly something strange/special that cannot be solved by this method

			// try out other directions gotten by findBestAxisVectors which should be u and v vectors defining the plane that gets defined by the normal that is the trace

			// change direction
			FVector traceDir = hit1.TraceEnd - hit1.TraceStart;
			FVector newDir1, newDir2;
			traceDir.FindBestAxisVectors(newDir1, newDir2);

			// the new directions may already be normalized, but to be sure
			newDir1.Normalize();
			newDir2.Normalize();
			
			//new dirs = average of normal and linedir rotated by 90 in two remaining axis
			dir1 = hit1.ImpactNormal + newDir1;//+ (random noise if it doesnt work?)
			dir2 = hit2.ImpactNormal + newDir1;
			dir1.Normalize();
			dir2.Normalize();

			// reset points
			movedImpactPoint1 = hit1.ImpactPoint;
			movedImpactPoint2 = hit2.ImpactPoint;

			if (!extendPointsUntilSight(movedImpactPoint1, movedImpactPoint2, dir1, dir2, traceParams, 100))
			{
				//try with newDir2 
				dir1 = hit1.ImpactNormal + newDir2;//+ (random noise if it doesnt work?)
				dir2 = hit2.ImpactNormal + newDir2;
				dir1.Normalize();
				dir2.Normalize();

				// reset points
				movedImpactPoint1 = hit1.ImpactPoint;
				movedImpactPoint2 = hit2.ImpactPoint;
				if (!extendPointsUntilSight(movedImpactPoint1, movedImpactPoint2, dir1, dir2, traceParams, 100))
				{
					// todo: maybe still try it with random noise
					return;
				}
			}
		}

		// found a traceable solution, save original impact points for later additional checks
		FVector originalImpactPoint1 = hit1.ImpactPoint + dir1 * 5;
		FVector originalImpactPoint2 = hit2.ImpactPoint + dir2 * 5;

		// moved out points to be added to hookline (note: order is important end->start!)
		TArray<FVector> newHookLinePoints;
		newHookLinePoints.Add(movedImpactPoint2);
		newHookLinePoints.Add(movedImpactPoint1);
		
		// TODO: only try optimization when we moved out the impactpoints more than once (or by distance of the points?)
		if (bTryToOptimizeResult)
		{
			// create a point on the line of the moved impact points to get it closer to the collision
			FVector halfLinePoint = movedImpactPoint1 + (movedImpactPoint2 - movedImpactPoint1) / 2.0;
			// check for the trace start/end to the halfLinePoint to dont intersect anything
			if (!World->LineTraceSingleByChannel(innerCollsion, hit1.TraceStart, halfLinePoint, ECC_GameTraceChannel1, traceParams) &&
				!World->LineTraceSingleByChannel(innerCollsion, hit1.TraceEnd, halfLinePoint, ECC_GameTraceChannel1, traceParams))
			{
				// the halflinepoint looks good from tracestart-end --> best case scenario: only add one point with no intersections near the corner
				newHookLinePoints.Empty();

				newHookLinePoints.Add(halfLinePoint);
				colCollisionCorrection1 = colCollisionCorrection2 = FColor::Emerald;
			}

			// check for the original impact points to the halfLinePoint to dont intersect anything
			else if (!World->LineTraceSingleByChannel(innerCollsion, originalImpactPoint1, halfLinePoint, ECC_GameTraceChannel1, traceParams) &&
				!World->LineTraceSingleByChannel(innerCollsion, originalImpactPoint2, halfLinePoint, ECC_GameTraceChannel1, traceParams))
			{
				// the halflinepoint looks good --> add the three new points into the hookline (from end to start)
				newHookLinePoints.Empty();

				newHookLinePoints.Add(originalImpactPoint2);
				newHookLinePoints.Add(halfLinePoint);
				newHookLinePoints.Add(originalImpactPoint1);
			}
		}

		for(int32 i = 0; i < newHookLinePoints.Num(); i++)
		{
			FVector curPoint = newHookLinePoints[i];
			mHookLinePoints.Insert(curPoint, indexToInsert);

			// create debug line for first and last point added
			// debug draw line from location to actual new hooklinePoint
			if(i == 0)
				DrawDebugLine(GetWorld(), hit1.ImpactPoint, movedImpactPoint1, colCollisionCorrection1, false, 30.0f, 0, 1.0f); 
			else if(i == newHookLinePoints.Num()-1)
				DrawDebugLine(GetWorld(), hit2.ImpactPoint, movedImpactPoint2, colCollisionCorrection2, false, 30.0f, 0, 1.0f);
		}
	}
}


bool AFPSHookProjectile::extendPointsUntilSight(FVector &pt1, FVector &pt2, FVector dir1, FVector dir2, FCollisionQueryParams traceParams, int32 maxIterations)
{
	UWorld* const World = GetWorld();
	if (World != NULL)
	{
		FHitResult hit;
		// move them out by 5unit steps and do raycast until that ray doesnt intersect anything anymore
		do
		{
			pt1 += dir1 * 5;
			pt2 += dir2 * 5;
			maxIterations--;
		} while (maxIterations && World->LineTraceSingleByChannel(hit, pt1, pt2, ECC_GameTraceChannel1, traceParams));

		// endless loop protection
		if (maxIterations)
			return true;
		else
			return false;
	}
	return false;
}


// calculate and apply physics to owner
void AFPSHookProjectile::ApplyPhysics(float DeltaSeconds)
{
	if (!mOwningActor || mHookLinePoints.Num() < 1)
		return;

	// add (a little more than) calculated outwardForce in the opposing direction (inward)
	AFPSHookCharacter* characterController = (AFPSHookCharacter*)mOwningActor;
	if (!characterController)
	{
		return;
	}

	FVector curPoint = mOwningActor->GetActorLocation();
	float winchingDir = 0.0f;

	float curLength = 0.0f;
	float curWinchingSpeed = 0.0f;
	for (int i = 0; i < mHookLinePoints.Num(); i++)
	{
		curLength += (curPoint - mHookLinePoints[i]).Size();
		curPoint = mHookLinePoints[i];
	}

	// apply winching (making line shorter or longer)
	if (mCurrentState == HOOK_STATE::WINCH_IN)
		winchingDir = -1.0f;
	else if (mCurrentState == HOOK_STATE::WINCH_OUT)
		winchingDir = 1.0f;

	if (winchingDir != 0.0f)
	{
		// calculate direction to pull the player in
		FVector pullDirection = mOwningActor->GetActorLocation() - mHookLinePoints[0];
		pullDirection.Normalize();

		// damping factor between 0 and 1 for the first 0.3 seconds
		float dampWinchStart = FMath::Clamp(mTimeWinch, 0.0f, 0.3f) / 0.3f;
		mTimeWinch += DeltaSeconds;
		curWinchingSpeed = mWinchInSpeed * dampWinchStart;

		// shorten/lengthen the line (winching in/out)
		mCurrentMaxLength += curWinchingSpeed * DeltaSeconds * winchingDir;
		if (mCurrentMaxLength < 0)
			mCurrentMaxLength = 0.0f;
	}

	if (curLength < mCurrentMaxLength)
	{
		// line is slack, so we dont restrict any movement
		mHookLineColor = FColor::Yellow;
	}
	else
	{
		// player out of bounds of the hook - pull him back in
		mHookLineColor = FColor::Red;
		
		// calculate direction to pull the player in
		FVector pullDirection = mHookLinePoints[0] - mOwningActor->GetActorLocation();
		pullDirection.Normalize();

		// calculate the velocity of the player against the pulling direction
		float outwardForce = FVector::DotProduct(mOwningActor->GetVelocity(), -pullDirection);
		float targetInwardForce = 0.0f;
		
		// apply the winching speed to the player (only if winching in)
		if (curWinchingSpeed * winchingDir < 0.0f)
		{
			// overwrite the normal inward velocity because we are winching in
			targetInwardForce = curWinchingSpeed;
			// maybe lower the inward velo by a bit because we overshoot when stopping winching in
			//targetInwardForce *= 0.8;
		}
		// give a bit of inward velocity (between 10 and 110)
		else
		{
			targetInwardForce = 10 + FMath::Clamp((curLength - mCurrentMaxLength) * 2.0f, 0.0f, 100.0f);
		}
		
		// apply forces in pullDirection, (targeted inward force) + (outwardforce correction, which may be negative because we are flying in pull direction already)
		if (-outwardForce < targetInwardForce)
		{
			characterController->GetCharacterMovement()->Velocity += (targetInwardForce + outwardForce) * pullDirection;

			// try to lift the restriction of z velocity when walking and winching in
			if (winchingDir < 0.0f && characterController->GetCharacterMovement()->MovementMode == EMovementMode::MOVE_Walking && characterController->GetCharacterMovement()->Velocity.Z > 0)
			{
				characterController->GetCharacterMovement()->MovementMode = EMovementMode::MOVE_Falling;
			}
		}

		// if we remove // decrease aircontrol it feels more natural but we cannot do cool jumps anymore
		//bla->GetCharacterMovement()->AirControl = 0.0f;
	}

	
}

// draw all parts of the hook line
void AFPSHookProjectile::DrawHookLine()
{
	if (!mOwningActor)
		return;

	float fThickness = 1.0f;
	if (mCurrentState == HOOK_STATE::FLYING)
	{
		DrawDebugLine(GetWorld(), mOwningActor->GetActorLocation(), this->GetActorLocation(), mHookLineColor, false, -1.0f, 0, fThickness);
	}
	else
	{
		FVector curPoint = mOwningActor->GetActorLocation();
		for (int i = 0; i < mHookLinePoints.Num(); i++)
		{
			DrawDebugLine(GetWorld(), curPoint, mHookLinePoints[i], mHookLineColor, false, -1.0f, 0, fThickness);
			curPoint = mHookLinePoints[i];
		}
	}
}

void AFPSHookProjectile::OnHit(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherActor!= mOwningActor) )
	{
		mHitActor = OtherActor;
		//OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
		CollisionComp->SetSimulatePhysics(false);
		ProjectileMovement->SetActive(false);

		// set state to hooked
		mCurrentState = HOOK_STATE::HOOKED;
		mTimeWinch = 0.0f;

		// inform hook character 
		AFPSHookCharacter* characterController = (AFPSHookCharacter*)mOwningActor;
		if (characterController)
			characterController->OnHookHit(this);
		
		// initial calculate line from point to player
		mHookLinePoints.Add(Hit.ImpactPoint + Hit.ImpactNormal * 5.0);
		//mHookLinePoints.Add(this->GetActorLocation());

		// TODO: implement recalcLine
		//float currentLength = RecalcLine();
		// for now recalc line only takes the point1-2 line
		FVector length = Hit.ImpactPoint - mOwningActor->GetActorLocation();
		float currentLength = length.Size();

		// init max length
		mCurrentMaxLength = currentLength;		
	}
}

void AFPSHookProjectile::SetHookState(HOOK_STATE state)
{
	// someone else is not allowed to deny the flying state
	if (mCurrentState == HOOK_STATE::FLYING)
		return;

	mCurrentState = state;

	// not in winching state -> reset winching time
	if (state != HOOK_STATE::WINCH_IN && state != HOOK_STATE::WINCH_OUT)
		mTimeWinch = 0.0f;
}

AFPSHookProjectile::HOOK_STATE AFPSHookProjectile::GetHookState()
{
	return mCurrentState;
}

void AFPSHookProjectile::SetOwner(AActor* OwningActor)
{
	mOwningActor = OwningActor;
}